import { IsNotEmpty, MinLength } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @MinLength(10)
  tel: string;

  @IsNotEmpty()
  gender: string;
}
